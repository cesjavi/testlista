﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace testlista1
{
    class itemAdapter : BaseAdapter
    {
        string[] items;

        Context context;

        public itemAdapter(Context context)
        {
            this.context = context;
        }

        public itemAdapter(Context context, string[] items)
        {
            this.context = context;
            this.items = items;
        }

        /*public override string this2[int position]
        {
            get { return items[position]; }
        }*/

        public override int Count
        {
            get { return items.Length; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            itemAdapterViewHolder holder = null;

            if (view != null)
                holder = view.Tag as itemAdapterViewHolder;

            if (holder == null)
            {
                holder = new itemAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();
                //replace with your item and your holder items
                //comment back in
                //view = inflater.Inflate(Resource.Layout.item, parent, false);
                view = inflater.Inflate(Resource.Layout.list_item, null);
                view.FindViewById<EditText>(Resource.Id.edtTexto).Text = items[position];
                view.FindViewById<Button>(Resource.Id.btnActivarGuardar).Text = "Cambiar " + position;
                //holder.Title = view.FindViewById<TextView>(Resource.Id.text);


                //view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
                //view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = items[position];
                view.Tag = holder;
            }


            //fill in your items
            //holder.Title.Text = "new text here";

            return view;
        }

        //Fill in cound here, currently 0
        /*public override int Count
        {
            get
            {
                return 0;
            }
        }*/

    }

    class itemAdapterViewHolder : Java.Lang.Object
    {
        //Your adapter views to re-use
        //public TextView Title { get; set; }
    }
}